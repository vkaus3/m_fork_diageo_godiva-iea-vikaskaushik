<%--

  <sortOptions.json.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================

  This will return the json for sort options dropdown.

  ==============================================================================

--%>
<%@ include file="/libs/foundation/global.jsp" %>

[

{
 "text":"Sort A to Z",
 "value":"ascAlphaNum"
 },
{
 "text":"Sort Z to A",
 "value":"descAlphaNum"
 },{
 "text":"Sort 1 to 9",
 "value":"ascNum"
 },
{
 "text":"Sort 9 to 1",
 "value":"descNum"
 }

]